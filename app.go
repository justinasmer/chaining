package main

import "fmt"

type App interface {
	Start()
	Stop()

	AddProvider(p Provider)

	SetPriorityStartChain(providers []Provider)
	SetPriorityStopChain(providers []Provider)
}

type app struct {
	providers []Provider

	startChain []Provider
	stopChain  []Provider
}

func NewApp() App {
	return &app{}
}

func (a *app) Start() {
	fmt.Println("Starting app")

	for _, p := range a.startChain {
		p.Start()
	}

	for _, p := range a.providers {
		p.Start()
	}
}

func (a *app) SetPriorityStartChain(chain []Provider) {
	a.startChain = chain
}

func (a *app) Stop() {
	fmt.Println("Stopping app")

	for _, p := range a.stopChain {
		p.Stop()
	}

	for _, p := range a.providers {
		p.Stop()
	}
}

func (a *app) SetPriorityStopChain(chain []Provider) {
	a.stopChain = chain
}

func (a *app) AddProvider(p Provider) {
	a.providers = append(a.providers, p)
}
