package main

import "fmt"

type Provider interface {
	Start()
	Stop()
	Wait()
}

type provider struct {
	started bool
	stopped bool

	name string
}

func NewProvider(name string) Provider {
	return &provider{name: name}
}

func (p *provider) Start() {
	if p.started {
		return
	}

	p.started = true

	fmt.Println("Started:", p.name)
}

func (p *provider) Stop() {
	if p.stopped {
		return
	}

	fmt.Println("Stopping:", p.name)
	p.Wait()
	fmt.Println("Stopped:", p.name)

	p.stopped = true
}

func (p *provider) Wait() {
	fmt.Println("Waiting for shutdown:", p.name)
}
