package main

import "time"

func main() {

	newApp := NewApp()

	providerA := NewProvider("A")
	providerB := NewProvider("B")
	providerC := NewProvider("C")

	newApp.AddProvider(providerA)
	newApp.AddProvider(providerB)
	newApp.AddProvider(providerC)

	newApp.SetPriorityStartChain([]Provider{providerB})
	newApp.SetPriorityStopChain([]Provider{providerC})

	newApp.Start()
	time.Sleep(time.Second * 5)

	newApp.Stop()
	time.Sleep(time.Second * 5)
}
